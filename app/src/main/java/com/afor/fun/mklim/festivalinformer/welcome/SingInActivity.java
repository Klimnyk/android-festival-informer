package com.afor.fun.mklim.festivalinformer.welcome;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.schedule.ScheduleActivity;
import com.afor.fun.mklim.festivalinformer.ui.AuthActivity;

public class SingInActivity extends AuthActivity implements View.OnClickListener {

    private static final String TAG = SingInActivity.class.getSimpleName();
    private SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singin);
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean activityStarted = prefs.getBoolean(TAG, false);
        if (activityStarted) {
            doNext(nextActivity());
        }
        findViewById(R.id.button_decline).setOnClickListener(this);
        findViewById(R.id.button_accept).setOnClickListener(this);
    }

    @Override
    protected Intent nextActivity() {
        return new Intent(this, ScheduleActivity.class);
    }

    @Override
    public void onClick(View view) {

        @SuppressLint("CommitPrefEdits")
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(TAG, true)
                .apply();
        if (view.getId() == R.id.button_accept) {
            signIn();
        } else {
            doNext(nextActivity());
        }
    }

}
