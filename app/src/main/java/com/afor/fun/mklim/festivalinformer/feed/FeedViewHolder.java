package com.afor.fun.mklim.festivalinformer.feed;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.wiget.HtmlTextView;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedViewHolder extends RecyclerView.Adapter<FeedViewHolder.ViewHolder> {

    private List<FeedMessage> list;
    private Context context;


    FeedViewHolder(List<FeedMessage> list) {
        this.list = list;
    }

    @Override
    public FeedViewHolder.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_message_card, parent, false);
        FeedViewHolder.ViewHolder vh = new FeedViewHolder.ViewHolder(v);
        context = v.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTitle.setText(list.get(position).getTitle());
        holder.mDescription.setHtmlText(list.get(position).getDescription());
        Glide.with(context)
                .load(list.get(position).getImageURL())
                .into(holder.mImage);
        holder.itemView.setOnClickListener(view ->
                Toast.makeText(context, list.get(position).getTitle()+ "click", Toast.LENGTH_SHORT).show());


    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView mTitle;
        @BindView(R.id.description)
        HtmlTextView mDescription;
        @BindView(R.id.image)
        ImageView mImage;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}