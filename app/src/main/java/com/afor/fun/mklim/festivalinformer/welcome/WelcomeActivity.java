package com.afor.fun.mklim.festivalinformer.welcome;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.afor.fun.mklim.festivalinformer.R;


public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = WelcomeActivity.class.getSimpleName();
    private SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean activityStarted = prefs.getBoolean(TAG, false);
        if (activityStarted) {
            doNext();
        }
        findViewById(R.id.button_accept).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

            @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(TAG, true)
                    .apply();
            doNext();
    }

    private void doNext() {
        Log.d(TAG, "Proceeding to next activity");
        Intent intent = new Intent(this, SingInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
