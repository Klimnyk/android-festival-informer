package com.afor.fun.mklim.festivalinformer.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.Menu;
import android.view.MenuItem;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.feed.FeedActivity;
import com.afor.fun.mklim.festivalinformer.info.InfoActivity;
import com.afor.fun.mklim.festivalinformer.map.MapActivity;
import com.afor.fun.mklim.festivalinformer.schedule.ScheduleActivity;

public abstract class BaseActivity extends AuthActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    protected BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewId());

        navigationView = findViewById(R.id.bottomNavigation);
        navigationView.setOnNavigationItemSelectedListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        updateNavigationBarState();
    }

    // Remove inter-activity transition to avoid screen tossing on tapping bottom navigation items
    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        navigationView.postDelayed(() -> {
            int itemId = item.getItemId();

            if (itemId != getNavigationMenuItemId()) {
                if (itemId == R.id.navigation_schedule) {
                    startActivity(new Intent(this, ScheduleActivity.class));
                } else if (itemId == R.id.navigation_feed) {
                    startActivity(new Intent(this, FeedActivity.class));
                } else if (itemId == R.id.navigation_map) {
                    startActivity(new Intent(this, MapActivity.class));
                } else if (itemId == R.id.navigation_info) {
                    startActivity(new Intent(this, InfoActivity.class));
                }
                finish();
            }
        }, 100);

        return true;


    }

    private void updateNavigationBarState() {
        int actionId = getNavigationMenuItemId();
        selectBottomNavigationBarItem(actionId);
    }

    void selectBottomNavigationBarItem(int itemId) {
        Menu menu = navigationView.getMenu();
        for (int i = 0, size = menu.size(); i < size; i++) {
            MenuItem item = menu.getItem(i);
            boolean shouldBeChecked = item.getItemId() == itemId;
            if (shouldBeChecked) {
                item.setChecked(true);
                break;
            }
        }
    }

    protected abstract int getContentViewId();

    protected abstract int getNavigationMenuItemId();
}
