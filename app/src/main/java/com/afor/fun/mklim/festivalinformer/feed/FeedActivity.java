package com.afor.fun.mklim.festivalinformer.feed;

import android.content.Intent;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.BaseActivity;

public class FeedActivity extends BaseActivity {

    @Override
    protected int getContentViewId() {
        return R.layout.activity_feed;
    }

    @Override
    protected int getNavigationMenuItemId() {
        return R.id.navigation_feed;
    }

    @Override
    protected Intent nextActivity() {
        return null;
    }
}
