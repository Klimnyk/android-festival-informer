package com.afor.fun.mklim.festivalinformer.schedule;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afor.fun.mklim.festivalinformer.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RareActivity extends AppCompatActivity {

    private String tag;

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.rating_description)
    TextView mDescription;

    @OnClick(R.id.send_review)
    void send() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            reference.child(getString(R.string.RARE_SCHEDULE_REFERENCE)).child(tag).child(user.getUid())
                    .setValue(String.valueOf(ratingBar.getRating())).addOnCompleteListener(task -> {
                if (task.isComplete()) {
                    Toast.makeText(this, R.string.is_complite_review, Toast.LENGTH_SHORT).show();
                    finish();
                }
                if (!task.isComplete()) {

                    Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }

            });
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rare);
        ButterKnife.bind(this);
        Intent intent = getIntent();

        String title = intent.getStringExtra("title");
        String description = intent.getStringExtra("description");
        tag = intent.getStringExtra("tag");

        ratingBar.setStepSize(1f);

        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        mToolbarTitle.setText(title);
        mDescription.setText(description);


    }
}
