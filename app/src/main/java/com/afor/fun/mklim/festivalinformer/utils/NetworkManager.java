package com.afor.fun.mklim.festivalinformer.utils;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkManager {
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm != null ? cm.getActiveNetworkInfo() : null) != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}