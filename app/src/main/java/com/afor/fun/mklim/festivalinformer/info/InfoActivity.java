package com.afor.fun.mklim.festivalinformer.info;

import android.content.Intent;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.BaseActivity;

public class InfoActivity extends BaseActivity {


    @Override
    protected int getContentViewId() {
        return R.layout.activity_info;
    }

    @Override
    protected int getNavigationMenuItemId() {
        return R.id.navigation_info;
    }

    @Override
    protected Intent nextActivity() {
        return null;
    }
}