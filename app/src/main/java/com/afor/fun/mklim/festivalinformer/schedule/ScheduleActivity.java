package com.afor.fun.mklim.festivalinformer.schedule;

import android.content.Intent;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.BaseActivity;

public class ScheduleActivity extends BaseActivity {

    @Override
    protected int getContentViewId() {
        return R.layout.activity_schedule;
    }

    @Override
    protected int getNavigationMenuItemId() {
        return R.id.navigation_schedule;
    }

    @Override
    protected Intent nextActivity() {
        return null;
    }
}