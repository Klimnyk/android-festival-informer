package com.afor.fun.mklim.festivalinformer.feed;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.wiget.Divider;
import com.afor.fun.mklim.festivalinformer.ui.wiget.StickyItemDecoration;
import com.afor.fun.mklim.festivalinformer.utils.FirebaseUtils;
import com.doctoror.particlesdrawable.ParticlesDrawable;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FeedFragment extends Fragment {

    private Long datasize;
    @BindView(R.id.feed_recycleview)
    RecyclerView mRecyclerView;
    private Unbinder unbinder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_feed, container, false);
        DatabaseReference reference = FirebaseUtils.getDatabase().getReference(getString(R.string.FEED_REFERENCE));
        unbinder = ButterKnife.bind(this, v);
        List<FeedMessage> data = new ArrayList<>();
        mRecyclerView.setHasFixedSize(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            ContextCompat.getDrawable(getContext(), R.drawable.particles_customized);
        else new ParticlesDrawable();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(mLayoutManager);
        FeedViewHolder mAdapter = new FeedViewHolder(data);
        mRecyclerView.addItemDecoration(new Divider(getContext()));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new StickyItemDecoration(getResources().getDimensionPixelSize(R.dimen.recycler_section_header_height),
                true, getSectionCallback(data)));
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    datasize = dataSnapshot.getChildrenCount();
                    data.add(postSnapshot.getValue(FeedMessage.class));
                    if (datasize == data.size()) {
                        Collections.sort(data, (o1, o2) -> Long.compare(o2.getTimestamp(), o1.getTimestamp()));
                        mAdapter.notifyDataSetChanged();
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return v;
    }

    private StickyItemDecoration.SectionCallback getSectionCallback(final List<FeedMessage> model) {
        return new StickyItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
                return position == 0
                        || !TextUtils.equals(getSimpleData(model.get(position)
                        .getTimestamp()), getSimpleData(model.get(position - 1)
                        .getTimestamp()));
            }

            @Override
            public CharSequence getSectionHeader(int position) {
                return getSimpleData(model.get(position)
                        .getTimestamp());
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    private String getSimpleData(Long timestamp) {
        @SuppressLint("SimpleDateFormat") String data = new SimpleDateFormat("d MMMM  HH:mm").format(timestamp);
        return data;
    }
}