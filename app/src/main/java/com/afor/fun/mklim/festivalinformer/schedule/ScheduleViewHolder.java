package com.afor.fun.mklim.festivalinformer.schedule;


import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.AuthActivity;
import com.afor.fun.mklim.festivalinformer.utils.NetworkManager;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleViewHolder extends RecyclerView.Adapter<ScheduleViewHolder.ViewHolder> {


    private final List<ScheduleModel> scheduleList;
    private Context context;

    ScheduleViewHolder(List<ScheduleModel> model) {
        this.scheduleList = model;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_session_item, parent, false);
        context = view.getContext();
        return new ScheduleViewHolder.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ScheduleModel model = scheduleList.get(position);
        holder.title.setText(model.getTitle());
        holder.description.setText(model.getDescription());
        holder.tag.setText(getResourceID(model.getTag(), "string", context));
        holder.tag.setBackgroundResource(getResourceID(model.getTag(), "color", context));
        holder.rare.setOnClickListener(view -> {
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {

                if (NetworkManager.isOnline(context)) {
                    Intent intent = new Intent(context, RareActivity.class);
                    intent.putExtra("title", model.getTitle());
                    intent.putExtra("description", model.getDescription());
                    intent.putExtra("tag", model.getId());
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, R.string.network_error, Toast.LENGTH_LONG).show();
                }

            } else {
                Snackbar.make(view, R.string.you_must_be_logged, Snackbar.LENGTH_SHORT)
                        .setAction(R.string.login, view1 -> {
                            if (context instanceof AuthActivity && NetworkManager.isOnline(context)) {
                                ((AuthActivity) context).signIn();
                            } else if (!NetworkManager.isOnline(context)) {
                                Toast.makeText(context, R.string.network_error, Toast.LENGTH_LONG).show();
                            }
                        })
                        .show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return scheduleList.size();
    }

    private int getResourceID
            (final String resName, final String resType, final Context ctx) {
        final int ResourceID =
                ctx.getResources().getIdentifier(resName, resType,
                        ctx.getApplicationInfo().packageName);
        if (ResourceID == 0) {
            throw new IllegalArgumentException
                    (
                            "No resource string found with name " + resName
                    );
        } else {
            return ResourceID;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.slot_title)
        TextView title;
        @BindView(R.id.slot_description)
        TextView description;
        @BindView(R.id.slot_tag)
        TextView tag;
        @BindView(R.id.rare_btn)
        Button rare;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}