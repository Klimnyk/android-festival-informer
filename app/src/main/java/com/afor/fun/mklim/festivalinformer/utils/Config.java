package com.afor.fun.mklim.festivalinformer.utils;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class Config {
    public final static String SCHEDULE_REFERENCE = "schedule";
    public final static String RARE_SCHEDULE_REFERENCE = "rare/schedule";
    public final static String FEED_REFERENCE = "feed";


    public static final int COLLAPSED_DESC_MAX_LINES = 3;
    public static final int EXPANDED_DESC_MAX_LINES = 30;

    public static class MapConfig {
        /*Camera setting*/
        public static final LatLng MAP_VIEWPORT_NW = new LatLng(48.352798, 29.512388);
        public static final LatLng MAP_VIEWPORT_SE = new LatLng(48.354675, 29.527582);
        public static final LatLng MAP_OVERLAY = new LatLng(48.354325, 29.519528);
        public static final LatLng MAP_DEFAULTCAMERA_TARGET = new LatLng(48.353969, 29.519442);
        public static final float MAP_DEFAULTCAMERA_ZOOM = 16f;
        /*Map setting*/
        public static final float MAP_MINZOOM = 16f;
        public static final float MAP_MAXZOOM = 17f;

        public static final int MAP_TYPE = GoogleMap.MAP_TYPE_NORMAL;

    /*Markers LatLng*/

        public static LatLng POSITION_MAIN_STAGE = new LatLng(48.354059, 29.514460);
        public static LatLng POSITION_FOOD_LOCATION = new LatLng(48.355052, 29.515531);
        public static LatLng POSITION_SPORT_LOCATION = new LatLng(48.354529, 29.517067);
        public static LatLng POSITION_ECO_LOCATION = new LatLng(48.353845, 29.516139);
        public static LatLng POSITION_CHILDREN_LOCATION = new LatLng(48.354251, 29.518136);
        public static LatLng POSITION_ACOUSTIC_STAGE = new LatLng(48.353282, 29.519825);
        public static LatLng POSITION_REGISTRATION_1 = new LatLng(48.354889, 29.512791);
        public static LatLng POSITION_REGISTRATION_2 = new LatLng(48.352830, 29.526944);
        public static LatLng POSITION_CAMPING = new LatLng(48.353742, 29.521543);
        public static LatLng POSITION_WC = new LatLng(48.354439, 29.521823);
        public static LatLng POSITION_SHOWER = new LatLng(48.353403, 29.521119);
        public static LatLng POSITION_PHOTOZONE = new LatLng(48.355154, 29.513533);
    }

}
