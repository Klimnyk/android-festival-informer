package com.afor.fun.mklim.festivalinformer.info;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.schedule.ViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class InfoFragment extends Fragment {

    private Unbinder unbinder;


    @BindView(R.id.viewpager)
    ViewPager viewPager;


    @BindView(R.id.tabs)
    TabLayout tabLayout;


    public InfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        unbinder = ButterKnife.bind(this, view);


        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new InfoViewPage(R.layout.info_view_page), getString(R.string.first_day));
        adapter.addFragment(new InfoViewPage(R.layout.info_view_page), getString(R.string.second_day));
        adapter.addFragment(new InfoViewPage(R.layout.info_view_page), getString(R.string.second_day));
        adapter.addFragment(new InfoViewPage(R.layout.info_view_page), getString(R.string.second_day));
        viewPager.setAdapter(adapter);
    }

}