package com.afor.fun.mklim.festivalinformer.feed;

public class FeedMessage {

    private Long timestamp;
    private String title;
    private String imageURL;
    private String description;
    private String id;

    public FeedMessage() {
    }

    public FeedMessage(Long timestamp, String title, String imageURL, String description, String id) {
        this.timestamp = timestamp;
        this.title = title;
        this.imageURL = imageURL;
        this.description = description;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getTitle() {
        return title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getDescription() {
        return description;
    }

}