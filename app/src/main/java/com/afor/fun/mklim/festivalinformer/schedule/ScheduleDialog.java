package com.afor.fun.mklim.festivalinformer.schedule;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.AuthActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ScheduleDialog extends DialogFragment {

    public ScheduleDialog() {
        // Required constructor.
    }

    public static ScheduleDialog newInstance() {
        return new ScheduleDialog();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        final Context context = getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        boolean signedIn = user != null;

        if (signedIn) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View titleView = inflater.inflate(R.layout.schedule_auth_dialog_signedin_title, null);
            TextView name = titleView.findViewById(R.id.name);
            TextView email = titleView.findViewById(R.id.email);
            final ImageView avatar = titleView.findViewById(R.id.avatar);

            name.setText(user.getDisplayName());
            email.setText(user.getEmail());

            // Note: this may be null if the user has not set up a profile photo.


            Glide.with(this)
                    .load(user.getPhotoUrl())
                    .apply(new RequestOptions().circleCrop()
                            .error(R.drawable.ic_account_circle_black_24dp)
                            .placeholder(R.drawable.ic_account_circle_black_24dp)
                            .fallback(R.drawable.ic_account_circle_black_24dp))
                    .into(avatar);

            builder.setCustomTitle(titleView);

            builder.setMessage(R.string.large_text_news);
        } else {
            builder.setMessage(R.string.large_text_news);
        }

        builder.setPositiveButton(signedIn ? R.string.logout : R.string.login,
                (dialog, which) -> {
                    AuthActivity authActivity = ((AuthActivity) getActivity());
                    if (signedIn) {
                        authActivity.signOut();
                    } else {

                        authActivity.signIn();
                    }
                    dismiss();
                });

        return builder.create();
    }

}
