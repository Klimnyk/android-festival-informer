package com.afor.fun.mklim.festivalinformer.schedule;


import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.utils.FirebaseUtils;
import com.afor.fun.mklim.festivalinformer.utils.NetworkManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.doctoror.particlesdrawable.ParticlesDrawable;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ScheduleFragment extends Fragment implements ScheduleRecycleView.Callbacks {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener stateListener;
    private List<ScheduleModel> listFirstDay;
    private List<ScheduleModel> listSecondDay;
    private Long listsize;
    private SharedPreferences prefs;
    private boolean databaseUpload;
    private Unbinder unbinder;


    @BindView(R.id.user_avatar)
    ImageView userAvatar;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.progress_bar_layout)
    FrameLayout progressBarLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @OnClick(R.id.user_avatar)
    void onClick() {
        FragmentManager fragmentManager = getFragmentManager();

        ScheduleDialog dialog = new ScheduleDialog();
        dialog.show(fragmentManager,"tera");



    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule, container, false);
        unbinder = ButterKnife.bind(this, view);

        mAuth = FirebaseAuth.getInstance();

        prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        databaseUpload = prefs.getBoolean("databaseUpload", false);

        listFirstDay = new ArrayList<>();
        listSecondDay = new ArrayList<>();

        stateListener = firebaseAuth -> updateUI();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            ContextCompat.getDrawable(getContext(), R.drawable.particles_customized);
        else new ParticlesDrawable();

        readDataFromFirebase();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(stateListener);
        updateUI();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void readDataFromFirebase() {

        if (!databaseUpload)
            if (!NetworkManager.isOnline(getContext())) {
                Toast.makeText(getContext(), R.string.network_error, Toast.LENGTH_LONG).show();
            }


        FirebaseDatabase database = FirebaseUtils.getDatabase();
        DatabaseReference myRef = database.getReference(getString(R.string.SCHEDULE_REFERENCE));

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                listsize = dataSnapshot.getChildrenCount();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    try {
                        if (TextUtils.equals(postSnapshot.getValue(ScheduleModel.class).getDay(), "second_day")) {
                            listSecondDay.add(postSnapshot.getValue(ScheduleModel.class));
                        } else if (TextUtils.equals(postSnapshot.getValue(ScheduleModel.class).getDay(), "first_day")) {
                            listFirstDay.add(postSnapshot.getValue(ScheduleModel.class));
                        }
                    } catch (DatabaseException e) {
                        FirebaseCrash.log(e.getMessage());
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                if (listFirstDay.size() + listSecondDay.size() == listsize) {

                    progressBar.setIndeterminate(false);
                    progressBarLayout.setVisibility(View.GONE);

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.putBoolean("databaseUpload", true)
                            .apply();

                    setupViewPager(viewPager);
                    tabLayout.setupWithViewPager(viewPager);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateUI() {
        if (this.isAdded()) {

            Uri avatarURI;
            try {
                avatarURI = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl();
            } catch (NullPointerException e) {
                avatarURI = Uri.parse("R.drawable.ic_account_circle_black_24dp");
            }

            Glide.with(this)
                    .load(avatarURI)
                    .apply(new RequestOptions().circleCrop()
                            .error(R.drawable.ic_account_circle_black_24dp)
                            .placeholder(R.drawable.ic_account_circle_black_24dp)
                            .fallback(R.drawable.ic_account_circle_black_24dp))
                    .into(userAvatar);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new ScheduleRecycleView(listFirstDay), getString(R.string.first_day));
        adapter.addFragment(new ScheduleRecycleView(listSecondDay), getString(R.string.second_day));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onRefreshData() {
//        refresh data from firebase

    }


    public interface Callbacks {
        void authUser();

        void logoutUser();
    }

}