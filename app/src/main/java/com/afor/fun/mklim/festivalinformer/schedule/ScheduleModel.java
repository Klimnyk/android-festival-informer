package com.afor.fun.mklim.festivalinformer.schedule;

public class ScheduleModel {
    private String day;
    private String title;
    private String time;
    private String description;
    private String tag;
    private Long timestamp;
    private String  id;


    public ScheduleModel() {

    }

    public ScheduleModel(String day, String title, String time, String description, String tag, Long timestamp, String id) {
        this.day = day;
        this.title = title;
        this.time = time;
        this.description = description;
        this.tag = tag;
        this.timestamp = timestamp;
        this.id = id;
    }



    public String getDay() {
        return day;
    }

    public String getTitle() {
        return title;
    }

    public String getTime() {
        return time;
    }

    public String getDescription() {
        return description;
    }

    public String getTag() {
        return tag;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getId() {
        return id;
    }
}