package com.afor.fun.mklim.festivalinformer.map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.BaseActivity;
import com.afor.fun.mklim.festivalinformer.utils.Config;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapActivity extends BaseActivity implements
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener, OnMapReadyCallback {


    /**
     * Area covered by the venue. Determines the viewport of the map.
     */
    private static final LatLngBounds VIEWPORT =
            new LatLngBounds(Config.MapConfig.MAP_VIEWPORT_NW, Config.MapConfig.MAP_VIEWPORT_SE);


    /**
     * Default position of the camera that shows the venue.
     */
    private static final CameraPosition VENUE_CAMERA =
            new CameraPosition.Builder()
                    .target(Config.MapConfig.MAP_DEFAULTCAMERA_TARGET)
                    .zoom(Config.MapConfig.MAP_DEFAULTCAMERA_ZOOM)
//                    .tilt(90f)
                    .build();

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;
        GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromAsset("maptiles/map_overlay.png"))
                .position(Config.MapConfig.MAP_OVERLAY, 1020f, 1020f);
        mMap.addGroundOverlay(newarkMap);


        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.google_style));
        mMap.setIndoorEnabled(true);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        mMap.setPadding(0, 120, 0, 0);

        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setCompassEnabled(true);


        mMap.setMinZoomPreference(Config.MapConfig.MAP_MINZOOM);
        mMap.setMaxZoomPreference(Config.MapConfig.MAP_MAXZOOM);
        mMap.setLatLngBoundsForCameraTarget(VIEWPORT);
        centerOnVenue();
        initializeMarkers();
    }

    private void initializeMarkers() {
        addMarker(Config.MapConfig.POSITION_REGISTRATION_1, R.string.map_marker_registration, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_PHOTOZONE, R.string.map_marker_photo_location, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_REGISTRATION_2, R.string.map_marker_registration_second, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_MAIN_STAGE, R.string.map_marker_main_stage, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_FOOD_LOCATION, R.string.map_marker_food_location, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_SPORT_LOCATION, R.string.map_marker_sport_location, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_ECO_LOCATION, R.string.map_marker_eco_location, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_CHILDREN_LOCATION, R.string.map_marker_children_location, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_ACOUSTIC_STAGE, R.string.map_marker_acoustic_stage, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_CAMPING, R.string.map_marker_camping, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_WC, R.string.map_marker_wc, R.mipmap.ic_launcher_round);
        addMarker(Config.MapConfig.POSITION_SHOWER, R.string.map_marker_shower, R.mipmap.ic_launcher_round);
    }

    private void addMarker(LatLng position, int title, int resID) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), resID);

        mMap.addMarker(new MarkerOptions()
                .position(position)
                .title(getString(title))
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromBitmap(bitmapSizeByScall(bm))));
    }


    /**
     * Moves the camera to the {@link #VENUE_CAMERA} positon.
     */
    private void centerOnVenue() {
        CameraUpdate camera = CameraUpdateFactory.newCameraPosition(VENUE_CAMERA);
        mMap.moveCamera(camera);
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), Config.MapConfig.MAP_MAXZOOM));
        return false;
    }

    @Override
    protected Intent nextActivity() {
        return null;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_maps;
    }

    @Override
    protected int getNavigationMenuItemId() {
        return R.id.navigation_map;
    }

    public Bitmap bitmapSizeByScall(Bitmap bitmapIn) {
        return Bitmap.createScaledBitmap(bitmapIn,
                Math.round(bitmapIn.getWidth() * 0.7f),
                Math.round(bitmapIn.getHeight() * 0.7f), false);
    }
}