package com.afor.fun.mklim.festivalinformer.schedule;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afor.fun.mklim.festivalinformer.R;
import com.afor.fun.mklim.festivalinformer.ui.wiget.Divider;
import com.afor.fun.mklim.festivalinformer.ui.wiget.StickyItemDecoration;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ScheduleRecycleView  extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static Callbacks sDummyCallbacks = () -> {};
    private Callbacks mCallbacks = sDummyCallbacks;

    private List<ScheduleModel> model;


    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.schedule_recycleview)
    RecyclerView recyclerView;
    private Unbinder unbinder;

    public ScheduleRecycleView() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ScheduleRecycleView(List<ScheduleModel> list) {
        this.model = list;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_schedule_recycleview, container, false);

        unbinder = ButterKnife.bind(this, view);

//        TODO hot fix db -7200000 milisec(2h)
        Collections.sort(model, (o1, o2) -> Long.compare(o1.getTimestamp()-7200000, o2.getTimestamp()-7200000));


        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        recyclerView.addItemDecoration(new StickyItemDecoration(getResources().getDimensionPixelSize(R.dimen.recycler_section_header_height),
                true, getSectionCallback(model)));
        recyclerView.addItemDecoration(new Divider(getContext()));
        recyclerView.setAdapter(new ScheduleViewHolder(model));
        // Refresh items
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.blue, R.color.purple, R.color.green, R.color.orange);

        return view;
    }

    @Override
    public void onRefresh() {
        mCallbacks.onRefreshData();
        (new Handler()).postDelayed(() -> mSwipeRefreshLayout.setRefreshing(false),20);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = sDummyCallbacks;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private StickyItemDecoration.SectionCallback getSectionCallback(final List<ScheduleModel> model) {
        return new StickyItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
//        TODO hot fix db -7200000 milisec(2h)
                return position == 0
                        || !TextUtils.equals(getSimpleData(model.get(position)
                        .getTimestamp()-7200000), getSimpleData(model.get(position - 1)
                        .getTimestamp()-7200000));
            }

            @Override
            public CharSequence getSectionHeader(int position) {
//        TODO hot fix db -7200000 milisec(2h)
                return getSimpleData(model.get(position)
                        .getTimestamp()-7200000);
            }
        };
    }


    public interface Callbacks {
        void onRefreshData();
    }
    private String getSimpleData(Long timestamp) {
        @SuppressLint("SimpleDateFormat") String data = new SimpleDateFormat("EEE HH:mm").format(timestamp);
        return data;
    }
}
